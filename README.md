Installing the project:

```
python -m pip install --upgrade pip
pip install -r requirements.txt

python -m spacy download en_core_web_sm
python -m spacy download ru_core_web_sm
python -m spacy download es_core_web_sm
python -m spacy download fr_core_web_sm

git clone https://github.com/alvations/gachalign en-ru/gachalign
```

For visualising the learning curve inside an experiment folder:
```
tensorboard --logdir=./runs
```