import os
data_folder = 'data'

data_path = os.path.join(data_folder, f'pubmed_en_es.txt')

train_path = os.path.join(data_folder, f'train_split.json')
valid_path = os.path.join(data_folder, f'valid_split.json')
manual_path = os.path.join(data_folder, f'manual.json')

train_path_preprocessed = os.path.join(data_folder, f'train_split_preprocessed.json')
valid_path_preprocessed = os.path.join(data_folder, f'valid_split_preprocessed.json')


train_path_preprocessed_ordered = os.path.join(data_folder, f'train_path_split_ordered.json')
valid_path_preprocessed_ordered = os.path.join(data_folder, f'valid_path_split_ordered.json')

