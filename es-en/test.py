from unittest import skip
import torch
import numpy as np
import nltk
from nltk.translate.bleu_score import sentence_bleu
from nltk.translate.meteor_score import meteor_score
from nltk.translate.chrf_score import sentence_chrf
import os
from glob import glob

from train import get_model_by_name, device
from proc import dump_the_json
from SpanishDataset import SpanishDataset
from transformers import MarianTokenizer
import pdb

def precision_recall_f1(decoder_sent, translated_sent):
    decoder_tokens = nltk.word_tokenize(decoder_sent)
    translated_tokens = nltk.word_tokenize(translated_sent)
    sent_intersection = list(set(decoder_tokens) & set(translated_tokens))
    precision = len(sent_intersection) / len(translated_tokens) if len(translated_tokens) > 0 else 0
    recall = len(sent_intersection) / len(decoder_tokens) if len(decoder_tokens) > 0 else 0
 
    f1_score = (precision * recall) / ( (precision + recall) / 2) \
                if len(sent_intersection) > 0 \
                else 0
    return precision, recall, f1_score

def dump_exp_pairs(original_es, translated_en_es, original_en, experiment_path):
    pair_list = []
    for original_es, translated_en_es, original_en in zip(original_es, translated_en_es, original_en):
        pair_list.append({
            "original_es": original_es,
            "translated_en_es": translated_en_es,
            "original_en": original_en
        })
    dump_the_json(pair_list, experiment_path)


def run_inference(model, dataset, config):
    tokenizer = MarianTokenizer.from_pretrained('Helsinki-NLP/opus-mt-en-es')

    precision_list, recall_list, f1_list = [], [], []
    bleu_list, chrf_list, meteor_list = [], [], []
    original_es, translated_en_es, original_en = [], [], []
    for i, entry in enumerate(dataset):
        encoder_ids, decoder_ids = entry
        encoder_ids = encoder_ids.to(device)
        decoder_ids = decoder_ids.to(device)

        with torch.no_grad():
            translated_ids = model.generate(encoder_ids.unsqueeze(0))[0]

        translated_sent = tokenizer.decode(translated_ids, skip_special_tokens=True)
        decoder_sent = tokenizer.decode(decoder_ids, skip_special_tokens=True)
        encoder_sent = tokenizer.decode(encoder_ids, skip_special_tokens=True)
        # print("The original sentance: "+ decoder_sent)
        # print("The translated sentance: " + translated_sent)
        # print("The sentance in english: " + encoder_sent)

        original_es.append(decoder_sent)
        translated_en_es.append(translated_sent)
        original_en.append(encoder_sent)

        precision, recall, f1_score = precision_recall_f1(decoder_sent, translated_sent)
        bleu_result = sentence_bleu([decoder_sent], translated_sent)
        chrf_result = sentence_chrf(decoder_sent, translated_sent)
        meteor_result = meteor_score([nltk.word_tokenize(decoder_sent)], nltk.word_tokenize(translated_sent))

        precision_list.append(precision)
        recall_list.append(recall)
        f1_list.append(f1_score)
        bleu_list.append(bleu_result)
        chrf_list.append(chrf_result)
        meteor_list.append(meteor_result)
        print(f"Iterating throught the dataset {i}/{len(dataset)}", end='\r')

    experiments_re = os.path.join('experiments', f'{config.get("model_name")}*')
    n_experments = len(glob(experiments_re))
    exp_dir = os.path.join('experiments', f'{config.get("model_name")}_{n_experments}')
    if not os.path.exists(exp_dir):
        os.makedirs(exp_dir)
    dump_exp_pairs(original_es, translated_en_es, original_en, os.path.join(exp_dir, 'output.json'))
    
    result = {
        "precision": np.mean(precision_list),
        "recall": np.mean(recall_list),
        "f1_score": np.mean(f1_list),
        "bleu": np.mean(bleu_list),
        "chrf": np.mean(chrf_list),
        "meteor": np.mean(meteor_list),

    }
    config.update(result)
    dump_the_json(config, os.path.join(exp_dir, 'result.json'))
    return result

def main():
    config = {  
        "model_name": "Helsinki-NLP/opus-mt-en-es",
        "weights_path": "logs/Helsinki-NLP/opus-mt-en-es_0/best.pth", 
        "batch_size": 32,
        "optimizer": "Adam",
        "n_epochs": 3,
    }

    model_name = config.get("model_name")
    model = get_model_by_name(model_name)

    # test_dataset = SpanishDataset('valid')

    # weights_path = config.get("weights_path")
    # if weights_path is not None:
        # state_dict = torch.load(weights_path)
        # model.load_state_dict(state_dict["model"])
    #     trained_metrics = run_inference(model, test_dataset)
    #     print(trained_metrics)

    test_dataset = SpanishDataset('manual')

    weights_path = config.get("weights_path")
    if weights_path is not None:
        state_dict = torch.load(weights_path)
        model.load_state_dict(state_dict["model"])
        trained_metrics = run_inference(model, test_dataset, config)
        print(trained_metrics)

if __name__ == "__main__":
    main()