from transformers import MarianMTModel
from torch.utils.data import DataLoader
from SpanishDataset import SpanishDataset
from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.utils.tensorboard import SummaryWriter
from torch.optim import Adam
from glob import glob
from nltk.translate.bleu_score import sentence_bleu

import torch
import os
import numpy as np
import pdb

device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

def get_lr(optimizerr):
    for param_group in optimizerr.param_groups:
        return param_group['lr']

def train_epoch(epoch, model, dataloaders, optimizerr, lr_sched, writer, exp_dir, best_loss):
    for phase in ['train', 'valid']:
        epoch_losses = []

        if phase == 'train':
            model.train()
        else:
            model.eval()

        for i, batch in enumerate(dataloaders[phase]):
            encoder_ids, decoder_ids = batch
            encoder_ids = encoder_ids.to(device)
            decoder_ids = decoder_ids.to(device)

            optimizerr.zero_grad()
            with torch.set_grad_enabled(phase == 'train'):
                # pdb.set_trace()
                output = model(input_ids=encoder_ids, labels=decoder_ids)
                loss = output.loss

                if phase == 'train':
                    loss.backward()
                    optimizerr.step()

            epoch_losses.append(loss.item())
            average_loss = np.mean(epoch_losses)
            lr = get_lr(optimizerr)

            if (i + 1) % 10 == 0:
                loading_percentage = int(100 * (i+1) / len(dataloaders[phase]))
                print(f'{phase}ing epoch {epoch}, iter = {i+1}/{len(dataloaders[phase])} ' + \
                    f'({loading_percentage}%), loss = {loss}, average_loss = {average_loss} ' + \
                    f'learning rate = {lr}', end='\r')
                
        if phase == 'valid' and average_loss < best_loss:
            best_loss = average_loss
            
            torch.save({
                    'model': model.state_dict(),
                    'optimizerrizer': optimizerr.state_dict(),
                    'lr_sched': lr_sched.state_dict(),
                    'epoch': epoch,
                    'validation_loss': best_loss
                }, os.path.join(exp_dir, 'best.pth'))
            
        if phase == 'train':
            metric_results = {
                'train_loss': average_loss
            }

            writer.add_scalar('Train/Loss', average_loss, epoch)
            writer.flush()

        if phase == 'valid':
            val_results = {
                'val_loss': average_loss
            }

            metric_results.update(val_results)
            writer.add_scalar('Test/Loss', average_loss, epoch)
            writer.flush()

            try:
                lr_sched.step()
            except:
                lr_sched.step(average_loss)
        
        print()
    return best_loss

def get_dataloaders(config):
    train_dataset = SpanishDataset('train')
    train_dataloader = DataLoader(train_dataset, batch_size=config["batch_size"], \
                                     shuffle=True, num_workers=4)
    test_dataset = SpanishDataset('valid')
    test_dataloader = DataLoader(test_dataset, batch_size=config["batch_size"], \
                                     shuffle=False, num_workers=4)
    return {
        'train': train_dataloader,
        'valid': test_dataloader
    }

def get_model_by_name(model_name):
    if model_name.startswith('Helsinki-NLP'):
        model = MarianMTModel.from_pretrained(model_name)
    return model.to(device)

def main():
    config = {
        "model_name": "Helsinki-NLP/opus-mt-en-es",
        "batch_size": 4,
        "optimizer": "Adam",
        "n_epochs": 5
    }

    dataloaders = get_dataloaders(config)

    model_name = config.get("model_name")
    model = get_model_by_name(model_name)

    if config["optimizer"] == "Adam":
        optimizerr = Adam(model.parameters(), lr=0.0001, betas=(0.9, 0.98), eps=1e-9)
    lr_sched = ReduceLROnPlateau(optimizerr, factor = 0.1, patience = 3, mode = 'min')

    experiments_re = os.path.join('logs', f'{model_name}*')
    n_experments = len(glob(experiments_re))
    exp_dir = os.path.join('logs', f'{model_name}_{n_experments}')

    if not os.path.exists(exp_dir):
        os.makedirs(exp_dir)
    writer = SummaryWriter(os.path.join(exp_dir, 'runs'))

    best_loss = 5000
    for epoch in range(config["n_epochs"]):
        best_loss = train_epoch(epoch, model, dataloaders, optimizerr, lr_sched, writer, exp_dir, best_loss)


if __name__ == "__main__":
    main()