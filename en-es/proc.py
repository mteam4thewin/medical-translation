from itertools import count
import spacy
import string
import json
import numpy as np
import io


from __init__ import train_path, valid_path, data_path
from sklearn.model_selection import train_test_split

ignored = ["I", "II", "III", "IV", "I.", "XXIII", "XVII",  "VI", "VII", "V", "VIII", "IX", "X", "XIV","XI", "XVII", "V.", "XX", "XVIII", "XVI", "XV", "XII", "X.", "XIII", "XXII","XII" ]

nlp_english = spacy.load("en_core_web_sm")
nlp_spanish = spacy.load("es_core_news_sm") 

def dump_the_json(pairs, filename):
    with open(filename, 'w', encoding="utf-8") as fout:
        json.dump(pairs, fout, ensure_ascii=False, indent=4)

def get_punctuation_cleaned(sentence, tool):
    tokens = [tok.text for tok in tool.tokenizer(sentence) 
                       if tok.text not in string.punctuation + ' ']
    sent_cleaned = ' '.join(tokens)
    return sent_cleaned.strip()
    
def get_clean_sents(text, language):  
    if language == 'es':
        nlp_tool = nlp_spanish
    else:
        nlp_tool = nlp_english

    sentences_cleaned = []
    inner_doc = nlp_tool(text)
    for inner_sent in inner_doc.sents:
        this_sent_cleaned = get_punctuation_cleaned(inner_sent.text.strip(), nlp_tool)
        if this_sent_cleaned != '':
            sentences_cleaned.append(this_sent_cleaned)
    return sentences_cleaned


def process_text(es_text, en_text):
    es_sents = get_clean_sents(es_text, language='es')
    en_sents = get_clean_sents(en_text, language='en')

    pair_list = []
    for es_sents, en_sents in zip(es_sents, en_sents):
        if (not es_sents in ignored) and (not en_sents in ignored) and ( np.abs(len(es_sents) - len(en_sents)) < 8 ) :
            pair_list.append({
                "es": es_sents,
                "en": en_sents
            })
    return pair_list


def main():
    
    paired_dataset = []
    counter = 0
 
    with open(data_path, 'r', encoding="utf-8") as f:
        for line in f:
            contents = line.split('|')
            idx = contents[0]
            en_text = contents[1].replace("[","").replace("]","")
            es_text = contents[2]        

            if en_text != "Not Available.":
                counter = counter + 1
                pair_list = process_text(es_text, en_text)
                if pair_list != None:
                    paired_dataset += pair_list
        print(counter)
            
    print(paired_dataset)
    train_pairs, valid_pairs, _, _ = train_test_split(paired_dataset, np.zeros(len(paired_dataset)), \
                                                      test_size=0.1, random_state=101)
    dump_the_json(train_pairs, train_path)
    dump_the_json(valid_pairs, valid_path)

if __name__ == "__main__":
    main()