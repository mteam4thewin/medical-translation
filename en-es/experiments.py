from operator import contains
from __init__ import train_path_preprocessed, valid_path_preprocessed, train_path, valid_path, train_path_preprocessed_ordered, valid_path_preprocessed_ordered
from proc import nlp_english, nlp_spanish, dump_the_json
from num2words import num2words
import readability
from collections import Counter
import numpy as np
import json
import re

special_tokens_mapping = {
    "≥": [" greater or equal than ", " mayor o igual que "], 
    "≤": [" lower or equal than ", " más baja o igual que "],
    ">": [" greater than ", " mas grande que "],
    "<": [" lower than ", " más baja que "],
    "%": [" percents ", " porcentajes "],
    "±": [" plus or minus ", " más o menos "],
    "=": [" equals ", " es igual "]
}

def get_float_form(token_str):
    try:
        float_token = float(token_str)
        return float_token
    except:
        return None


def get_int_form(token_str):
    try:
        int_token = int(token_str)
        return int_token
    except:
        return None

def explain_special_tokens(to_explain):
    """ erases numbers with comma and transforms numbers separated by dash with a between flag: "1-5" becomes "between 1 and 5" 
    """
    results = re.findall(r'\d+\,\d+', to_explain)
    for result in results:
        new_token = result.replace(',', '.')
        to_explain = to_explain.replace(result, new_token)

    results = re.findall(r'(?:\d+\.)*\d+\-\d+(?:\.\d+)*', to_explain)
    for result in results:
        first, second = result.split('-')
        new_token = f'between {first} and {second}'
        to_explain = to_explain.replace(result, new_token)
    return to_explain

def explain_regular_tokens(token_str, lang):
    """ explains regular tokens consisting of int, float numbers or special characters
    """
    token_equiv = special_tokens_mapping.get(token_str)
    float_equiv = get_float_form(token_str)
    int_equiv = get_int_form(token_str)
    
    if float_equiv is not None and '.' in token_str:
        int_part, frac_part = str(float_equiv).split('.')
        int_rep = num2words(int_part, lang=lang[:2])
        frac_rep = num2words(frac_part, lang=lang[:2])
        return f'{int_rep} point {frac_rep}'

    elif int_equiv is not None:
        token_explained = num2words(token_str, lang=lang[:2])
        return token_explained

    elif token_equiv is not None:
        position = 0 if lang == 'en' else 1
        return token_equiv[position]
    else:
        return token_str

def get_explained_str(to_explain, lang):
    nlp_tool = nlp_english if lang == 'en' else nlp_spanish

    to_explain = explain_special_tokens(to_explain)

    tokens = nlp_tool.tokenizer(to_explain)
    tokens_rep = []
    for token in tokens:
        token_explained = explain_regular_tokens(str(token), lang)
        tokens_rep.append(token_explained)
    sentence_rep = ' '.join(tokens_rep)
    return sentence_rep

def get_complexity(e):
    try:
        results = readability.getmeasures(e['en'], lang='en')
        return results['readability grades']['FleschReadingEase']
    except:
        return 10^6


def main(scope):
    json_path = train_path if scope == "train" else valid_path
    json_path_preprocessed = train_path_preprocessed if scope == "train" else valid_path_preprocessed
    json_path_preprocessed_ordered = train_path_preprocessed_ordered if scope == "train" else valid_path_preprocessed_ordered
    with open(json_path, 'r', encoding="utf-8") as fin:
        sentence_list = json.load(fin)

    for i, entry in enumerate(sentence_list):
        for lang in ["en", "es"]:
            entry[lang] = get_explained_str(entry[lang], lang)           
        print(f"Explaining progress {i}/{len(sentence_list)}", end='\r')
    print()
    dump_the_json(sentence_list, json_path_preprocessed)
    sentence_list.sort(reverse=True, key=get_complexity)
    dump_the_json(sentence_list, json_path_preprocessed_ordered)

if __name__ == "__main__":
    main('train')
    main('valid')