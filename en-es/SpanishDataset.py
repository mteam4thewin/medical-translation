from asyncio import ensure_future
from torch.utils.data import Dataset
from __init__ import train_path, valid_path, manual_path
from transformers import MarianTokenizer

import json
import pdb

class SpanishDataset(Dataset):
    def __init__(self, scope, max_len=65):
        super().__init__()

        if scope == 'train':
            self.json_path = train_path
        elif scope == 'valid':
            self.json_path = valid_path
        elif scope == 'manual':
            self.json_path = manual_path

        with open(self.json_path, 'r', encoding="utf=8") as fin:
            self.pairs = json.load(fin)
    
        self.tokenizer = MarianTokenizer.from_pretrained('Helsinki-NLP/opus-mt-es-en')
        self.max_len = max_len
    
    def __len__(self):
        return len(self.pairs)

    def __getitem__(self, index):
        entry = self.pairs[index]

        encoder_ids = self.tokenizer.encode(entry['es'], return_tensors='pt', truncation=True, \
                                            padding='max_length', max_length=self.max_len)[0]
        decoder_ids = self.tokenizer.encode(entry['en'], return_tensors='pt', truncation=True, \
                                            padding='max_length', max_length=self.max_len)[0]
        return encoder_ids, decoder_ids
