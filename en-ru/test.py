from unittest import result
import torch
import numpy as np

from train import get_model_by_name, device
from proc import dump_the_json
from RussianDataset import RussianDataset
from transformers import MarianTokenizer

from nltk.translate.bleu_score import sentence_bleu, corpus_bleu
from nltk.translate.meteor_score import meteor_score
from nltk.translate.chrf_score import sentence_chrf
from nltk.tokenize import word_tokenize
import pdb
import json
import os
import time

def precision_recall_f1(decoder_sent, translated_sent):
    decoder_tokens = word_tokenize(decoder_sent)
    translated_tokens = word_tokenize(translated_sent)

    sent_intersection = list(set(decoder_tokens) & set(translated_tokens))
    precision = len(sent_intersection) / len(translated_tokens) if len(translated_tokens) > 0 else 0
    recall = len(sent_intersection) / len(decoder_tokens) if len(decoder_tokens) > 0 else 0

    f1_score = (precision * recall) / ( (precision + recall) / 2) \
                if len(sent_intersection) > 0 \
                else 0
    return precision, recall, f1_score

def run_metrics(comparing_list):
    precision_list, recall_list, f1_list = [], [], []
    bleu_list, chrf_list, meteor_list = [], [], []

    prep_refs = []
    prep_hypo = []
    for i, entry in enumerate(comparing_list):
        decoder_sent = entry['grand_truth']
        translated_sent = entry['translated']

        prep_hypo.append(decoder_sent)
        prep_refs.append([translated_sent])
    
        precision, recall, f1_score = precision_recall_f1(decoder_sent, translated_sent)
        bleu_result = sentence_bleu([decoder_sent], translated_sent)
        chrf_result = sentence_chrf(decoder_sent, translated_sent)
        meteor_result = meteor_score([word_tokenize(decoder_sent)], word_tokenize(translated_sent))

        precision_list.append(precision)
        recall_list.append(recall)
        f1_list.append(f1_score)
        bleu_list.append(bleu_result)
        chrf_list.append(chrf_result)
        meteor_list.append(meteor_result)

        print(f"Iterating throught the dataset {i}/{len(comparing_list)}", end='\r')
    print()

    
    return {
        "precision": np.mean(precision_list),
        "recall": np.mean(recall_list),
        "f1_score": np.mean(f1_list),
        "bleu": np.mean(bleu_list),
        "corpus_bleu": corpus_bleu(prep_refs, prep_hypo),
        "chrf": np.mean(chrf_list),
        "meteor": np.mean(meteor_list)
    }


def run_inference(model, dataset, reverse=False):
    tokenizer = MarianTokenizer.from_pretrained('Helsinki-NLP/opus-mt-en-ru') \
                if reverse \
                else MarianTokenizer.from_pretrained('Helsinki-NLP/opus-mt-ru-en')

    start_time = time.perf_counter()
    comparing_list = []
    for i, entry in enumerate(dataset):
        encoder_ids, encoder_sent, decoder_sent = entry
        encoder_ids = encoder_ids.to(device)

        with torch.no_grad():
            translated_ids = model.generate(encoder_ids.unsqueeze(0))[0]

        translated_sent = tokenizer.decode(translated_ids, skip_special_tokens=True)

        comparing_list.append({
            "input_": encoder_sent,
            "grand_truth": decoder_sent,
            "translated": translated_sent
        })
        elapsed = time.perf_counter() - start_time
        print(f"Iterating throught the dataset {i}/{len(dataset)}, {elapsed:.2f} seconds elapsed", end='\r')
    print()
    return comparing_list
    

def compute_comparing_list(weights_path, phase, model, reverse):
    exp_dir = os.path.dirname(weights_path)
    test_dataset = RussianDataset(phase, return_full=True, reverse=reverse)
    
    results_path = os.path.join(exp_dir, f'{phase}_results.json')
    if not os.path.exists(results_path):
        state_dict = torch.load(weights_path)
        model.load_state_dict(state_dict["model"])

        comparing_list = run_inference(model, test_dataset, reverse)
        dump_the_json(comparing_list, results_path)
    else:
        with open(results_path, 'r', encoding='utf-8') as fin:
            comparing_list = json.load(fin)
    return comparing_list

def main():
    config = {
        "model_name": "Helsinki-NLP/opus-mt-ru-en",
        "weights_path": "logs/Helsinki-NLP/opus-mt-en-ru_0/best.pth",
        "batch_size": 4,
        "optimizer": "Adam",
        "n_epochs": 16,
        "phase": "valid"
    }

    model_name = config.get("model_name")
    model = get_model_by_name(model_name)
    
    weights_path = config.get("weights_path")
    if weights_path is None:
        return None
    for phase in ["valid", "test"]:
        comparing_list = compute_comparing_list(weights_path, phase, model, reverse=True)
        trained_metrics = run_metrics(comparing_list)
        print(phase, trained_metrics)

if __name__ == "__main__":
    main()