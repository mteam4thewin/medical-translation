import os
import pdb
import spacy
import string
import numpy as np
import json

from glob import glob
from __init__ import txt_folder, train_path, valid_path
from sklearn.model_selection import train_test_split

from gachalign.gale_church import align

nlp_english = spacy.load("en_core_web_sm")
nlp_russian = spacy.load("ru_core_news_sm")

english_markers = ['OBJECTIVE', 'FINDINGS', 'CONCLUSION', 'SHORT TERM', 'OUTCOMES', \
                   'RESULTS', 'AIM', 'MATERIAL', 'METHODS', 'PURPOSE', 'PATIENT', \
                   'CLINICAL CASE', 'LESSONS LEARNT', 'AND', 'RELEVANT CHANGES', 'LOCAL SETTING', 'SUBJECTS']
russian_markers = ['Резюме', 'Цель', 'Результаты', 'Вывод', 'Заключение', 'Материал и методы', 'Методы', 'исследования']  

def read_from_file(filename):
    with open(filename, 'r', encoding="utf-8") as fin:
        lines = fin.read().split('\n')
    return lines

def dump_the_json(pairs, filename):
    with open(filename, 'w', encoding='utf-8') as fout:
        json.dump(pairs, fout, indent=4, ensure_ascii=False)


def get_punctuation_cleaned(sentence, tool):
    tokens = [tok.text for tok in tool.tokenizer(sentence) 
                       if tok.text not in string.punctuation + ' ']
    sent_cleaned = ' '.join(tokens)
    return sent_cleaned.strip()
    
def get_clean_sents(txt_filename, language):
    raw_sents = read_from_file(txt_filename)
    sents = []
    for sent in raw_sents:
        # splits by extra delimiters
        sents += sent.split(':')

    if language == 'ru':
        nlp_tool = nlp_russian
        blacklist = russian_markers
    else:
        nlp_tool = nlp_english
        blacklist = english_markers

    sentences_cleaned = []
    for sent in sents:
        inner_doc = nlp_tool(sent)
        for inner_sent in inner_doc.sents:
            this_sent_cleaned = get_punctuation_cleaned(inner_sent.text.strip(), nlp_tool)
            
            suspected = 0
            for marker in blacklist:
                if marker.lower() in this_sent_cleaned.lower():
                    suspected += 1

            if len(this_sent_cleaned.split()) - suspected > 2:
                sentences_cleaned.append(this_sent_cleaned)
    return sentences_cleaned

def extract_file_id(rus_filename):
    basename = os.path.basename(rus_filename)
    file_id = basename.split('_')[0]
    return file_id

def proces_files(rus_filename, eng_filename):
    rus_sents = get_clean_sents(rus_filename, language='ru')
    eng_sents = get_clean_sents(eng_filename, language='en')

    pair_list = []
    for rus_sent, eng_sent in align(rus_sents, eng_sents):
        if rus_sent != '' and eng_sent != '':
            pair_list.append({
                "rus": rus_sent,
                "eng": eng_sent
            })
    return pair_list

def main():
    paired_dataset = []
    rus_files_re = os.path.join(txt_folder, '*_ru.txt')
    rus_filenames = glob(rus_files_re)
    for idx, rus_filename in enumerate(rus_filenames):
        basename = os.path.basename(rus_filename)
        txt_id = basename.split('_')[0]
        eng_filename = os.path.join(txt_folder, f'{txt_id}_en.txt')

        pair_list = proces_files(rus_filename, eng_filename)
        paired_dataset += pair_list
        print(f"Processing progress {idx}/{len(rus_filenames)} ", end='\r')

    train_pairs, valid_pairs, _, _ = train_test_split(paired_dataset, np.zeros(len(paired_dataset)), \
                                                      test_size=0.1, random_state=101)
    dump_the_json(train_pairs, train_path)
    dump_the_json(valid_pairs, valid_path)

if __name__ == "__main__":
    main()