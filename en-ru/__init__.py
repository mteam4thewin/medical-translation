import os
txt_folder = 'ru-en-release'
data_folder = 'data'

train_path = os.path.join(data_folder, f'train_split.json')
valid_path = os.path.join(data_folder, f'valid_split.json')
test_path = os.path.join(data_folder, f'test_split.json')