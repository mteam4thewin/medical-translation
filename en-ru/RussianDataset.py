from audioop import reverse
from torch.utils.data import Dataset
from __init__ import train_path, valid_path, test_path
from transformers import MarianTokenizer

import json
import pdb

class RussianDataset(Dataset):
    def __init__(self, scope, max_len=150, return_full=False, reverse=False):
        super().__init__()

        if scope == 'train':
            self.json_path = train_path
        elif scope == 'valid':
            self.json_path = valid_path
        else:
            self.json_path = test_path

        with open(self.json_path, 'r', encoding="utf-8") as fin:
            self.pairs = json.load(fin)
    
        self.tokenizer = MarianTokenizer.from_pretrained('Helsinki-NLP/opus-mt-en-ru') \
                         if reverse \
                         else MarianTokenizer.from_pretrained('Helsinki-NLP/opus-mt-ru-en')
        self.max_len = max_len
        self.return_full = return_full
        self.reverse = reverse
    
    def __len__(self):
        return len(self.pairs)

    def __getitem__(self, index):
        entry = self.pairs[index]

        encoder_ids = self.tokenizer.encode(entry['rus'], return_tensors='pt', truncation=True, \
                                            padding='max_length', max_length=self.max_len)[0]
        decoder_ids = self.tokenizer.encode(entry['eng'], return_tensors='pt', truncation=True, \
                                            padding='max_length', max_length=self.max_len)[0]
        
        if self.return_full:
            if self.reverse:
                return decoder_ids, entry['eng'], entry['rus']
            return encoder_ids, entry['rus'], entry['eng']
        
        return encoder_ids, decoder_ids
