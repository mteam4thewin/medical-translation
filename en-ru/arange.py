from __init__ import train_path, valid_path, test_path
from proc import nlp_english, nlp_russian, dump_the_json, get_punctuation_cleaned
from num2words import num2words
import readability

import pdb
import json
import re
import time

special_tokens_mapping = {
    "≥": ["greater or equal than", "больше или равно"], 
    "≤": ["lower or equal than", "меньше или равно"],
    ">": ["greater than", "лучше чем"],
    "<": ["lower than", "ниже чем"],
    "%": ["percents", "проценты"],
    "±": ["plus or minus", "плюс или минус"],
    "=": ["equals", "равно"]
}


def get_float_form(token_str):
    try:
        float_token = float(token_str)
        return float_token
    except:
        return None

def get_int_form(token_str):
    try:
        int_token = int(token_str)
        return int_token
    except:
        return None

def explain_special_tokens(to_explain):
    """ erases numbers with comma and transforms numbers separated by dash with a between flag: "1-5" becomes "between 1 and 5" 
    """
    results = re.findall(r'\d+\,\d+', to_explain)
    for result in results:
        new_token = result.replace(',', '.')
        to_explain = to_explain.replace(result, new_token)

    results = re.findall(r'(?:\d+\.)*\d+\-\d+(?:\.\d+)*', to_explain)
    for result in results:
        first, second = result.split('-')
        new_token = f'between {first} and {second}'
        to_explain = to_explain.replace(result, new_token)
    return to_explain

def replace_special_symbols(to_explain, lang):
    """ replace the scientific symbols from special_tokens_mapping with their equivalent
    """
    position = 0 if lang == 'eng' else 1
    for key, value in special_tokens_mapping.items():
        to_explain = to_explain.replace(key, f' {value[position]} ')
    return to_explain

def explain_regular_tokens(token_str, lang):
    """ explains regular tokens consisting of int, float numbers or special characters
    """
    float_equiv = get_float_form(token_str)
    int_equiv = get_int_form(token_str)
    
    if float_equiv is not None and '.' in token_str:
        int_part, frac_part = f'{float_equiv:f}'.split('.')
        int_rep = num2words(int_part, lang=lang[:2])
        frac_rep = num2words(frac_part, lang=lang[:2])
        return f'{int_rep} point {frac_rep}'

    elif int_equiv is not None:
        token_explained = num2words(token_str, lang=lang[:2])
        return token_explained
    else:
        return token_str

def get_explained_str(to_explain, lang):
    nlp_tool = nlp_english if lang == 'eng' else nlp_russian

    to_explain = explain_special_tokens(to_explain)
    to_explain = replace_special_symbols(to_explain, lang)

    tokens = nlp_tool.tokenizer(to_explain)
    tokens_rep = []
    for token in tokens:
        token_explained = explain_regular_tokens(str(token), lang)
        tokens_rep.append(token_explained)
    sentence_rep = ' '.join(tokens_rep)
    return sentence_rep


def demo():
    str_to_explain = '≥ 5 years was 1.1 (95% CI: 0.4-2.0) and 1.3 (95% CI: 0.5-2.2) per 10,000'
    explained = get_explained_str(str_to_explain, lang='eng')
    # try the russian example as well
    print(str_to_explain)
    print(explained)

def get_flesch_reading_ease_eng(text):
    if text == '':
        return 1e6
    results = readability.getmeasures(text, lang='en')
    return results['readability grades']['FleschReadingEase']

def apply_curriculum_sort(sentence_list):
    start_time = time.perf_counter()
    sentence_list.sort(key=lambda entry: get_flesch_reading_ease_eng(entry['eng']), reverse=True)
    
    time_elapsed = time.perf_counter() - start_time
    print(f"Curriculum sorting finished in {time_elapsed} seconds")

def main(scope):
    if scope == "train":
        json_path = train_path
    elif scope == "valid":
        json_path = valid_path
    else:
        json_path = test_path
    
    with open(json_path, 'r', encoding="utf-8") as fin:
        sentence_list = json.load(fin)

    for i, entry in enumerate(sentence_list):
        for lang in ["eng", "rus"]:
            if scope == 'test':
                tool = nlp_english if lang == "eng" else nlp_russian
                entry[lang] = get_punctuation_cleaned(entry[lang], tool)
            entry[lang] = get_explained_str(entry[lang], lang)
        print(f"Explaining progress {i}/{len(sentence_list)}", end='\r')
    print()
    
    if scope == "train":
        apply_curriculum_sort(sentence_list)
    dump_the_json(sentence_list, json_path)

if __name__ == "__main__":
    main('train')
    main('valid')
    main('test')
    
    # TODO: complete the paper as well: 
    # optional experiments - a score for the pretrained model, en-ru score, another optimizer 
